from math import log

import cobra
import pandas as pd

from pytfa.optim import GenericConstraint

from pytfa.io import load_thermoDB, read_lexicon, annotate_from_lexicon, read_compartment_data, apply_compartment_data
from typing import List
import pytfa

# from me.rela.experiment import RelativeLC

from cobra import Model, Reaction, Metabolite

class Relative_Metabolomics_Experiment(object):
    '''
    This was originally "base_model in reframed"

    '''
    def __init__(self, base_model: Model = None, data: pd.DataFrame = None, id: str = ''):
        self.id = id
        self.base_model = base_model
        self.base_data = data

    def size(self):
        pass

    @property
    def integrated_model(self):
        if self._merged_model is None:
            self._merged_model = self.merge_models()

        return self._merged_model

    def merge_models(self):
        comm_model = CBModel(self.id)
        old_ext_comps = []
        ext_mets = []
        self.reaction_map = {}
        self.metabolite_map = {}

        # default IDs
        ext_comp_id = "ext"
        biomass_id = "community_biomass"
        comm_growth = "community_growth"

        # create external compartment

        comp = Compartment(ext_comp_id, "extracellular environment", external=True)
        comm_model.add_compartment(comp)

        # community biomass

        met = Metabolite(biomass_id, "Total community biomass", ext_comp_id)
        comm_model.add_metabolite(met)

        rxn = CBReaction(comm_growth, name="Community growth rate",
                         reversible=False, stoichiometry={biomass_id: -1},
                         lb=0, ub=inf, objective=1)

        comm_model.add_reaction(rxn)

        # add each organism

        for org_id, model in self.organisms.items():

            def rename(old_id):
                return f"{old_id}_{org_id}"

            # add internal compartments

            for c_id, comp in model.compartments.items():
                if comp.external:
                    old_ext_comps.append(c_id)
                else:
                    new_comp = Compartment(rename(c_id), comp.name)
                    comm_model.add_compartment(new_comp)

            # add metabolites

            for m_id, met in model.metabolites.items():
                if met.compartment not in old_ext_comps:  # if is internal
                    new_id = rename(m_id)
                    new_met = Metabolite(new_id, met.name, rename(met.compartment))
                    new_met.metadata = met.metadata.copy()
                    comm_model.add_metabolite(new_met)
                    self.metabolite_map[(org_id, m_id)] = new_id

                elif m_id not in comm_model.metabolites:  # if is external but was not added yet
                    new_met = Metabolite(m_id, met.name, ext_comp_id)
                    new_met.metadata = met.metadata.copy()
                    comm_model.add_metabolite(new_met)
                    ext_mets.append(new_met.id)

            # add genes

            for g_id, gene in model.genes.items():
                new_id = rename(g_id)
                new_gene = Gene(new_id, gene.name)
                new_gene.metadata = gene.metadata.copy()
                comm_model.add_gene(new_gene)

            # add internal reactions

            for r_id, rxn in model.reactions.items():

                if rxn.reaction_type == ReactionType.EXCHANGE:
                    continue

                new_id = rename(r_id)
                new_stoichiometry = {
                    m_id if m_id in ext_mets else rename(m_id): coeff
                    for m_id, coeff in rxn.stoichiometry.items()
                }

                if r_id == model.biomass_reaction:
                    new_stoichiometry[biomass_id] = 1

                if rxn.gpr is None:
                    new_gpr = None
                else:
                    new_gpr = GPRAssociation()
                    new_gpr.metadata = rxn.gpr.metadata.copy()

                    for protein in rxn.gpr.proteins:
                        new_protein = Protein()
                        new_protein.genes = [rename(g_id) for g_id in protein.genes]
                        new_protein.metadata = protein.metadata.copy()
                        new_gpr.proteins.append(new_protein)

                new_rxn = CBReaction(
                    new_id,
                    name=rxn.name,
                    reversible=rxn.reversible,
                    stoichiometry=new_stoichiometry,
                    reaction_type=rxn.reaction_type,
                    lb=rxn.lb,
                    ub=rxn.ub,
                    gpr_association=new_gpr
                )

                comm_model.add_reaction(new_rxn)
                new_rxn.metadata = rxn.metadata.copy()
                self.reaction_map[(org_id, r_id)] = new_id

        # Add exchange reactions

        for m_id in ext_mets:
            r_id = f"R_EX_{m_id[2:]}" if m_id.startswith("M_") else f"R_EX_{m_id}"
            rxn = CBReaction(r_id, reversible=True, stoichiometry={m_id: -1},
                             reaction_type=ReactionType.EXCHANGE)
            comm_model.add_reaction(rxn)

        return comm_model

    def replicate_model(self) -> cobra.Model:
        if self.base_model is None or self.base_data is None:
            raise RuntimeError("Relative Metabolomics Data Model needs both a base model and Experimental data")

        model_out = Model('ok')
        # model_out.genes = base_model.genes.copy() NO NEED FOR NOW
        for _, case in enumerate(self.base_data.columns):
            met_case_suffix = '.case' + case

            for met in self.base_model.metabolites:
                # create duplicate
                met_tmp = Metabolite(
                    met.id + met_case_suffix
                    ,
                    formula=met.formula,
                    name=met.name + ' for case ' + case,
                    compartment=met.compartment,
                    charge=met.charge
                )
                met_tmp.notes = met.notes
                met_tmp.annotation = met.annotation
                met_tmp.original_case_id = met.id

                model_out.add_metabolites([met_tmp])

            model_out.repair()

            for reac in self.base_model.reactions:
                reaction = Reaction(reac.id + '.case' + case)
                reaction.name = reac.name + ' for case ' + case
                reaction.subsystem = reac.subsystem
                reaction.lower_bound = reac.lower_bound
                reaction.upper_bound = reac.upper_bound
                reaction.annotation = reac.annotation
                reaction.gene_reaction_rule = reac.gene_reaction_rule

                yeah = {model_out.metabolites.get_by_id(k.id + met_case_suffix): v for k, v in reac.metabolites.items()}
                reaction.add_metabolites(yeah)
                model_out.add_reactions([reaction])
                
        return model_out

    def tfa_convert(model_tmp: Model, path_to_repos: str = './../../../repos/pytfa') -> pytfa.ThermoModel:
        thermo_data = load_thermoDB(f'{path_to_repos}/data/thermo_data.thermodb')
        lexicon = read_lexicon(f'{path_to_repos}/models/iJO1366/lexicon.csv')
        comp_data = read_compartment_data(f'{path_to_repos}/models/iJO1366/compartment_data.json')

        # Initialize the cobra_model
        mytfa = pytfa.ThermoModel(thermo_data, model_tmp)

        # Annotate the cobra_model
        annotate_from_lexicon(mytfa, lexicon)
        apply_compartment_data(mytfa, comp_data)

        mytfa.name = 'tutorial_basics'

        # Solver settings

        def apply_solver_settings(model, solver='gurobi'):
            model.solver = solver
            # base_model.solver.configuration.verbosity = 1
            model.solver.configuration.tolerances.feasibility = 1e-9
            if solver == 'gurobi':
                model.solver.problem.Params.NumericFocus = 3
            model.solver.configuration.presolve = True

        apply_solver_settings(mytfa)

        ## TFA conversion
        mytfa.prepare()
        mytfa.convert()  # add_displacement = True)

        ## Info on the cobra_model
        mytfa.print_info()

        return mytfa

    def add_relative_metabolomic_constraints(self, replicated_model: pytfa.ThermoModel, df,
                                             mets_to_add_rel=['3pg_c', '2pg_c']):
        # todo: have this method take in the relative metabolomics df

        for met_to_add_rel in mets_to_add_rel:

            # TODO: DETERMINE ALL THE CASE PAIRS
            for case_pair in [['A', 'B'], ['A', 'C']]:
                LC_ChemMet = 0

                # Formulate the constraint
                for case, sign in zip(case_pair, [1, -1]):
                    coef = df.loc[met_to_add_rel, case]
                    met_case_suffix = '.case' + case
                    LC_ChemMet += (
                            replicated_model.LC_vars[replicated_model.metabolites.get_by_id(met_to_add_rel + met_case_suffix)]  # self!
                            * sign - sign * log(coef)
                    )

                # TODO: ADD ERROR
                replicated_model.add_constraint(RelativeLC, replicated_model, LC_ChemMet, lb=0, ub=0,
                                                id_='_'.join(case_pair + [met_to_add_rel]))  # SELF!

        return replicated_model

    def old_main(self):

        pass
        # model_tmp = replicate_model(model, cases)

        # mytfa = tfa_convert(model_tmp)


class RelativeLC(GenericConstraint):
    """
    Class to represent relative contraints between LogConcentration variables

    RelLC: StoichCoefProd1 * LC_prod1 - StoichCoefProd2 * LC_prod2 = 0
    """
    prefix = 'RelLC_'

    def __init__(self, model, expr, **kwargs):
        GenericConstraint.__init__(self, expr=expr, model=model, hook=None, **kwargs)