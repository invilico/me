# ME

This repo contains metabolic engineering work done in collaboration with 
University of Cadiz Laboratory of Bioreactors and Enzyme Catalysis.

Let's fucking rock n' roll.

## To install, run the following
```mkdir repos
cd repos/
git clone https://github.com/opencobra/cobrapy.git
git clone https://github.com/EPFL-LCSB/pytfa.git
git clone https://github.com/cdanielmachado/reframed.git
git clone https://github.com/opencobra/optlang.git
cd pytfa/
git lfs install
git lfs pull
cd ../.. 
 
git clone <THIS REPO>

then run a command to activate the pipenv (pipenv install)
```

The above hasn't been tested. If you want to do this feel free to let me know how it works



### from pipenv file

#### pipenv
make sure git-lfs is installed on system

```
mkdir model
cd model/
pipenv --python 3.8
cd ./..

mkdir repos
cd repos/
git clone https://github.com/opencobra/cobrapy.git
git clone https://github.com/EPFL-LCSB/pytfa.git
cd pytfa/
git lfs install
git lfs pull
cd ../../model

pipenv install -e ../repos/cobrapy/
pipenv install -e ../repos/pytfa/
```

installed jupyter lab extensions on escher doc site


## From pip file

make sure git-lfs is installed on system
```
mkdir cobra_rel
cd cobra_rel/
python3 -m venv cobra_env
mkdir repos
cd repos/
git clone https://github.com/opencobra/cobrapy.git
git clone https://github.com/EPFL-LCSB/pytfa.git
cd pytfa/
git lfs install
git lfs pull
cd ../..
source cobra_env/bin/activate
pip install -e repos/cobrapy/
pip install -e repos/pytfa/
```