# Calculate rates
# The first task is to estimate specific rates for the exchange fluxes that we know


# import necessary packages
import warnings

import numpy
import pandas as pd
import xarray
from cobra.io import load_json_model
from typing import List


def calculate_specific_rates(path_to_data: str, timepoints: List[dict], conf: dict):
    # import the measurements and put in a dataframe
    input_data = pd.read_csv(path_to_data)
    input_data.time = pd.to_numeric(input_data.time, errors='coerce')
    input_data.value = pd.to_numeric(input_data.value, errors='coerce')
    tmp = []
    for timepoint in timepoints:
        time_slice = input_data[input_data.time.isin(timepoint['points'])]
        output = points_to_specific_rates(time_slice, timepoint['time'])
        tmp.append(output)
        calc_C_closure(output, conf['base_model_path'])

    result = pd.concat(tmp, ignore_index=True)
    result.to_csv(conf['output_path'])


def points_to_specific_rates(snapshot: pd.DataFrame, time: int) -> pd.DataFrame:
    snapshot = snapshot.dropna()
    # lets fit a curve to each combination of 1. molecule and 2. strain
    groups = snapshot.groupby(['molecule', 'strain'])
    flat = groups.apply(lambda group: pd.Series({'curve': numpy.polyfit(group.time, group.value, 2)})).reset_index()
    # using the curve, calculate value and rate of production at time point.
    flat['value_at'] = flat.apply(lambda row: numpy.poly1d(row['curve'])(time), axis=1)
    flat['rate'] = flat.apply(lambda row: numpy.poly1d(row['curve']).deriv(1)(time), axis=1)
    if any(flat.rate[flat.molecule == 'CDW'] < 0):  # we don't want negative growth
        flat.rate[flat.molecule == 'CDW'] = flat.rate[flat.molecule == 'CDW'].clip(lower=0)
        warnings.warn('Negative growth rates being set to 0')
    u_at_time = flat.query('molecule == "CDW"')[['strain', 'value_at']].rename(columns={"value_at": "biomass"})
    flat = flat.merge(u_at_time, how='outer')
    flat['specific_rate'] = flat.rate.div(flat.biomass)
    flat['t'] = time
    print(flat)
    return flat


def calc_C_closure(flat: pd.DataFrame, base_model_path):
    # let's look at overall carbon closure

    # TODO: generalize this to get C balance from any base_model:
    # get the carbon stoichiometry of the growth reaction and other molecules
    base_model = load_json_model(base_model_path)
    biomass_cmol = abs(base_model.reactions.BIOMASS_Ecoli_core_w_GAM.check_mass_balance()["C"])
    cmol = pd.DataFrame({'cmol': [biomass_cmol, 3, 4],
                         'molecule': ['CDW', 'glyc', 'succ']})

    for name, df in flat.groupby('strain'):
        together = df[['molecule', 'specific_rate']].merge(cmol, how='outer')
        together = together.assign(c_rate=together.specific_rate * together.cmol)
        print(name, together)
        c_r = pd.Series(together['c_rate'].values, index=together['molecule'])
        print(f'Carbon Closure: {c_r.CDW:.2f} + {c_r.succ:.2f} / {c_r.glyc:.2f} = {-(c_r.CDW + c_r.succ) / c_r.glyc:.0%}')