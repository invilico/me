
from gekko import GEKKO
from numpy import exp, log


#Initialize Model
m = GEKKO()

#define parameter
vc = m.Param(value=4)
vp = m.Param(value=1)
vt = m.Param(value=5)

#initialize variables
LCa,LCb = [m.Var(lb=log(1e-7), ub=log(1e-1)) for i in range(2)]
LCa_c,LCa_p,LCb_c,LCb_p = [m.Var(lb=log(1e-7), ub=log(1e-1)) for i in range(4)]

#initial values
LCa.value = 1e-4
LCa_c.value = 1e-4
LCa_p.value = 1e-4
LCb_c.value = 1e-4
LCb_p.value = 1e-4
LCb.value = 1e-4

#Equations
m.Equation(LCa==log(5)+LCb)
m.Equation(m.exp(LCa) == vc/vt*m.exp(LCa_c) + vp/vt*m.exp(LCa_p))
m.Equation(m.exp(LCb) == vc/vt*m.exp(LCb_c) + vp/vt*m.exp(LCb_p))

#Objective
m.Obj(-LCb_c)

#Set global options
m.options.IMODE = 3 #steady state optimization

#Solve simulation
solved = m.solve()

#Results
print('')
print('Results')
print('LCa: ' + str(LCa.value))
print('LCa_c: ' + str(LCa_c.value))
print('LCa_p: ' + str(LCa_p.value))
print('LCb: ' + str(LCb.value))
print('LCb_c: ' + str(LCb_c.value))
print('LCb_p: ' + str(LCb_p.value))
