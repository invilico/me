# create the json version of the matlab base_model

from pytfa.io import import_matlab_model
# this was only possible by changing numpy int16 to int, a change that has since then been merged
# this is the reggem base_model that comes is included in pytfa 2020/05/12
cobra_model = import_matlab_model('.../small_ecoli.mat')
from cobra.io import save_json_model
save_json_model(cobra_model, 'redgem.json')
