
# This example considers the following nonconvex nonlinear problem

# maximize 2x +y
# subject to 
# exp(x) + 4 sqrt(y) <= 9
# x, y >= 0


import math
import gurobipy as gp 
from gurobipy import GRB

def printsol(m, x, y, u, v):
    print('x = ' + str(x.x) + ', u = ' + str(u.x))
    print('y = ' + str(y.x) + ', v = ' + str(v.x))
    print('Obj = ' + str(m.objVal))

    # Calculate violation of exp(x) + 4 sqrt(y) <= 9
    vio = math.exp(x.x) + 4 * math.sqrt(y.x) - 9
    if vio < 0:
        vio = 0
    print('Vio = ' + str(vio))




# Create a new base_model
m = gp.Model()
# Create variables
x = m.addVar(name='x')
y = m.addVar(name='y')
u = m.addVar(name='u')
v = m.addVar(name='v')

# Set objective
m.setObjective(2*x + y, GRB.MAXIMIZE)
# Add constraints
lc = m.addConstr(u + 4*v <= 9)


# u = exp(x)
gcf1 = m.addGenConstrExp(x, u, name="gcf1")
# v = x^(0.5)
gcf2 = m.addGenConstrPow(y, v, 0.5, name="gcf2")
# Use the equal piece length approach with the length = 1e-3
m.params.FuncPieces = 1
m.params.FuncPieceLength = 1e-3
# Optimize the base_model
m.optimize()

printsol(m, x, y, u, v)