

redgem_map = 'redgem_map_200503.json'
if create_escher_map:

    # get the reactions already in the escher map
    import json
    with open(redgem_map, "r") as read_file:
        redgem_temp = json.load(read_file)
    in_map = [reac['bigg_id'] for reac in redgem_temp[1]['reactions'].values()]

    # check the reactions in the base_model
    from cobra.io import load_json_model
    redgem = load_json_model('redgem.json')


    import pandas as pd
    from pytfa.thermo.utils import is_exchange, check_transport_reaction


    df = pd.DataFrame(
        [reac.id, is_exchange(reac), check_transport_reaction(reac), reac.build_reaction_string()] for reac in redgem.reactions if reac.id not in in_map
    )
    df.columns = ['id', 'exchange', 'transport', 'formula']

    df.query('exchange==0 & transport== 0 & ~id.str.contains("LMPD")')