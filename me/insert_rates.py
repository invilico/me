
# - Inserting experimental flux measurements for each strain


# Here we need take into account two steps before we get 3 functional models.
# 1. When we insert the specific rates into the base_model, **we multiply them by 10**. Why? The base_model has a "maintenance"
# reaction (that represents the basal energy consumed to sustain life without growth). This maintenance value is
# calculated assuming 10 mmol Glucose/h/gCDW.  We scale all of our fluxes so that the carbon input is roughly consistent
# with this assumption.

# 2. Second, after we insert our bounds we **may find that the base_model is infeasible**. Try inserting the exact flux
# values (10x) for M4 gnd. You will find that the base_model is infeasible meaning that the base_model cannot stoichiometrically
# sustain the defined fluxes. Thus one solution is to **relax the imposed flux bounds** to a certain percentage error.
# Expand the bounds slowly until you find a working distribution.


import cobra
import numpy
import pandas
from cobra.exceptions import OptimizationError
from optlang.symbolics import Zero


def get_metabolite_exhange_reac(string: str, model: cobra.core.model.Model) -> cobra.core.reaction.Reaction:
    # shitty function
    return [reac for reac in model.metabolites.query(string + '_e')[0].reactions if reac.boundary][0]

def apply_specific_rates_loosen(inmodel : cobra.core.model.Model, rates: pandas.DataFrame):

    model = inmodel.copy()

    for err in numpy.linspace(0, 0.3, num=301):
        for rate in rates.itertuples():
            r = rate.specific_rate
            m = rate.molecule

            if rate.molecule == 'CDW':
                model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = (r - abs(r) * err ) * 10
                model.reactions.BIOMASS_Ecoli_core_w_GAM.upper_bound = (r + abs(r) * err ) * 10
            else:
                reac = get_metabolite_exhange_reac(m, model)
                reac.lower_bound = (r - abs(r) * err ) * 10
                reac.upper_bound = (r + abs(r) * err ) * 10

        try:
            model.optimize(raise_error=True)
            break
        except OptimizationError:
            pass
    print(err)

    return model


def append_flexibility(df: pandas.DataFrame, suf: str) -> pandas.DataFrame:
    return df.assign(diff=df.maximum - df.minimum) \
        .rename(lambda v: v[0:3] + suf, axis='columns')  # rel=(df.maximum-df.minimum)/abs(df.maximum+df.minimum)


def apply_fva_bounds(model: cobra.core.model.Model, df: pandas.DataFrame):
    model_out = model.copy()
    for reac_tuple in df.round(6).itertuples():
        model_out.reactions.get_by_id(reac_tuple[0]).bounds = reac_tuple[1:3]
    return model_out


# calc geometric diy fba
def diy_geo(model, fva_sol):
    with model:
        # Variables' and constraints' storage variables.
        consts = []
        obj_vars = []
        updating_vars_cons = []
        prob = model.problem
        mean_flux = (fva_sol["maximum"] + fva_sol["minimum"]).abs() / 2

        # Set the gFBA constraints.
        for rxn in model.reactions:
            var = prob.Variable("geometric_fba_" + rxn.id,
                                lb=0,
                                ub=mean_flux[rxn.id])
            upper_const = prob.Constraint(rxn.flux_expression - var,
                                          ub=mean_flux[rxn.id],
                                          name="geometric_fba_upper_const_" +
                                               rxn.id)
            lower_const = prob.Constraint(rxn.flux_expression + var,
                                          lb=mean_flux[rxn.id],
                                          name="geometric_fba_lower_const_" +
                                               rxn.id)
            updating_vars_cons.append((rxn.id, var, upper_const, lower_const))
            consts.extend([var, upper_const, lower_const])
            obj_vars.append(var)
            model.add_cons_vars(consts)

            # Minimize the distance between the flux distribution and center.
            model.objective = prob.Objective(Zero, sloppy=True, direction="min")
            model.objective.set_linear_coefficients({v: 1.0 for v in obj_vars})
            # Update loop variables.
            sol = model.optimize()
        return sol


def fva_sort(df: pandas.DataFrame, geoflux, suf: str) -> pandas.DataFrame:
    return df.assign(diff=df.maximum - df.minimum,
                     geo=geoflux.fluxes,
                     #                     rel=(df.maximum-df.minimum)/abs(df.maximum+df.minimum)
                     ) \
        .rename(lambda v: v[0:3] + suf, axis='columns')
