#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytfa

from optlang.exceptions import SolverError

from cobra.core.model import SolverNotFound
from cobra.flux_analysis import flux_variability_analysis
from cobra.io import load_matlab_model, load_json_model

from pytfa.io import import_matlab_model, load_thermoDB, \
    read_lexicon, annotate_from_lexicon, \
    read_compartment_data, apply_compartment_data

GLPK = 'optlang-glpk'
solver = GLPK

# Load reaction DB
print("Loading thermo data...")

# DFHG mod
dfhg_path = './../../repos/pytfa/data/thermo_data.thermodb'
# thermo_data = load_thermoDB('../data/thermo_data.thermodb')
thermo_data = load_thermoDB(dfhg_path)


print("Done !")

# We import pre-compiled data as it is faster for bigger models
cobra_model = load_json_model('../models/e_coli_core.json')
lexicon = read_lexicon('../repos/pytfa/models/iJO1366/lexicon.csv')
compartment_data = read_compartment_data('../repos/pytfa/models/iJO1366/comp_data.json')

# Initialize the cobra_model
mytfa = pytfa.ThermoModel(thermo_data, cobra_model)

# Annotate the cobra_model
annotate_from_lexicon(mytfa, lexicon)
apply_compartment_data(mytfa, compartment_data)

biomass_rxn = 'BIOMASS_Ecoli_core_w_GAM'

mytfa.name = 'tutorial_basics'
mytfa.solver = solver
mytfa.objective = biomass_rxn


# Solver settings

def apply_solver_settings(model, solver=solver):
    model.solver = solver
    # base_model.solver.configuration.verbosity = 1
    model.solver.configuration.tolerances.feasibility = 1e-9
    if solver == 'optlang_gurobi':
        model.solver.problem.Params.NumericFocus = 3
    model.solver.configuration.presolve = True


apply_solver_settings(mytfa)

## FBA
fba_solution = cobra_model.optimize()
fba_value = fba_solution.objective_value
# fva = flux_variability_analysis(mytfa)

## TFA conversion
mytfa.prepare()
mytfa.convert()  # add_displacement = True)

## Info on the cobra_model
mytfa.print_info()

## Optimality
tfa_solution = mytfa.optimize()
tfa_value = tfa_solution.objective_value

# It might happen that the base_model is infeasible. In this case, we can relax
# thermodynamics constraints:

# Report
print('FBA Solution found : {0:.5g}'.format(fba_value))
print('TFA Solution found : {0:.5g}'.format(tfa_value))
