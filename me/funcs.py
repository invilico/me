from cobra import Model
from cobra.io import load_json_model

from pytfa.io import load_thermoDB, read_lexicon, annotate_from_lexicon, read_compartment_data, apply_compartment_data

import pytfa

def create_toy_model() -> Model:
    cobra_model = load_json_model('models/e_coli_core.json')
    model = Model('funky')

    model.add_reactions([cobra_model.reactions.PGM, cobra_model.reactions.ENO])
    model.add_reactions([cobra_model.reactions.H2Ot, cobra_model.reactions.EX_h2o_e])

    model.add_boundary(model.metabolites[0], type='sink')
    model.add_boundary(model.metabolites[1], type='sink')
    model.add_boundary(model.metabolites[3], type='sink')

    model.reactions.ENO.objective_coefficient = 1

    # Verify if the reaction can carry flux and is reversible
    assert (model.optimize(objective_sense='minimize').objective_value < 0)
    assert (model.optimize(objective_sense='maximize').objective_value > 0)

    return model

def tfa_convert(model_tmp: Model, path_to_repos: str = '', solver='gurobi') -> pytfa.ThermoModel:
    thermo_data = load_thermoDB(f'{path_to_repos}data/from_pytfa/thermo_data.thermodb')
    lexicon = read_lexicon(f'{path_to_repos}data/from_pytfa/lexicon.csv')
    comp_data = read_compartment_data(f'{path_to_repos}data/from_pytfa/compartment_data.json')

    # Initialize the cobra_model
    mytfa = pytfa.ThermoModel(thermo_data, model_tmp)

    # Annotate the cobra_model
    annotate_from_lexicon(mytfa, lexicon)
    apply_compartment_data(mytfa, comp_data)

    mytfa.name = 'tutorial_basics'

    # Solver settings

    def apply_solver_settings(model, solver):
        model.solver = solver
        # base_model.solver.configuration.verbosity = 1
        model.solver.configuration.tolerances.feasibility = 1e-9
        if solver == 'gurobi':
            model.solver.problem.Params.NumericFocus = 3
        model.solver.configuration.presolve = True

    apply_solver_settings(mytfa, solver)

    ## TFA conversion
    mytfa.prepare()
    mytfa.convert()  # add_displacement = True)

    ## Info on the cobra_model
    mytfa.print_info()

    return mytfa