

# What does this folder do

Antonio's lab has produced 3 mutant strains and measured the production/consumption of Succinic Acid and Glycerol as well as growth, for each strain. We will use modelling to explore differences in the intracellular fluxes occuring in each strain, and hopefully generate hypotheses that will lead to greater production.

To accomplish this task we must proceed through the following steps:

0. Convert experimental measurements into specific rates that we can integrate into the model (we did this the first day)
1. Download the E. coli core model and install cobrapy as working tools
2. Insert Glycerol Metabolism into the model
3. Correct default Exchange fluxes (defining the medium)
4. Create 3 strain specific models (including inserting reactions)
5. Insert experimental flux measurements for each strain from step 0.
6. Perform flux variability analysis for each strain (by going back and enlargening flux bounds in step 5.)
7. Compare the flux ranges for each strain

