import os
from unittest import TestCase


from math import e


import pandas as pd
import pandas.testing as pd_testing

from pytfa.analysis.variability import variability_analysis
from pytfa.optim import LogConcentration, DeltaG

from me.funcs import create_toy_model, tfa_convert
from me.rela.experiment import Relative_Metabolomics_Experiment as RelExp

class Experiment(TestCase):
    def assertDataframeEqual(self, a, b, msg):
        try:
            pd_testing.assert_frame_equal(a, b)
        except AssertionError as e:
            raise self.failureException(msg) from e

    def setUp(self) -> None:
        self.addTypeEqualityFunc(pd.DataFrame, self.assertDataframeEqual)

    def test_toy_relative_experiment(self):

        #### duplicate base_model
        model = create_toy_model()

        # TODO: import relative metabolomic data
        data = pd.DataFrame({'A': [1000, 1], 'B': [10, 100], 'C': [1, 2000]}, index=['3pg_c', '2pg_c'])

        # initialize the experiment
        toytest = RelExp(model,data)

        model_tmp = toytest.replicate_model()

        mytfa = tfa_convert(model_tmp, solver='optlang-glpk')

        fva = variability_analysis(mytfa, kind=['reactions', LogConcentration, DeltaG])

        mytfa_w_relative_metabo = toytest.add_relative_metabolomic_constraints(mytfa, data)


        # simulate and print out
        fva1 = variability_analysis(mytfa_w_relative_metabo, kind=['reactions', LogConcentration, DeltaG])

        pgm_cases = ['PGM.caseA', 'PGM.caseB', 'PGM.caseC']
        print(fva1.loc[pgm_cases, :])
        print(fva1.loc[['LC_2pg_c.caseA', 'LC_2pg_c.caseB', 'LC_2pg_c.caseC'], :])
        print(fva1.loc[['LC_3pg_c.caseA', 'LC_3pg_c.caseB', 'LC_3pg_c.caseC'], :])

        ok = mytfa_w_relative_metabo.optimize()
        sol_conc = ok.raw[['LC_3pg_c.caseA', 'LC_3pg_c.caseB', 'LC_3pg_c.caseC', 'LC_2pg_c.caseA', 'LC_2pg_c.caseB',
                       'LC_2pg_c.caseC']].rpow(e)
        print(sol_conc)

        cols = ['minimum', 'maximum']
        df2 = pd.DataFrame([[-1000, 0], [-1000, 1000], [0, 1000]], columns=cols, index=pgm_cases, dtype='float64')
        self.assertEqual(fva1.loc[pgm_cases, cols], df2)

        df3 = pd.DataFrame([[-1000, 1000], [-1000, 1000], [-1000, 1000]], columns=cols, index=pgm_cases, dtype='float64')
        self.assertEqual(fva.loc[pgm_cases, cols], df3)


        df3 = pd.DataFrame([[-1000, 1000], [-1000, 1000], [-1000, 1000]], columns=cols, index=pgm_cases, dtype='float64')
        self.assertEqual(fva.loc[pgm_cases, cols], df3)